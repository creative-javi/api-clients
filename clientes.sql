CREATE DATABASE ApiClientsDB;
use ApiClientsDB;
CREATE TABLE IF NOT EXISTS Clientes (
    idCliente Int AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(250) NOT NULL,
    edad Int(2) NOT NULL,
    email VARCHAR(250) NULL,
    estatus BOOL NOT NULL,
    PRIMARY KEY (idCliente)
);

INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Javier Garcia', '22','javier.creative.apps@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Ivan Estrada', '22','i.estrada@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Juan Mendoza', '23','j.francisco.m@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Natalia Velez', '21','nat.vel@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Jose Negrete', '21','jose.negrete@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Saul Negrete', '22','snegrete@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Horacio Arriaga', '22','harriaga@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Martin Herrera', '22','mherrera@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Erick Herrera', '22','eherrera@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Alicia Ortiz', '26','alice.ortiz@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Francisco Garcia', '22','francisco.gh.2012@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Miguel Torres', '22','mtorres@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Angel Medrano', '22','mmedrano@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Vicente Plancarte', '24','vplan@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Norberto Zavala', '25','zavala.garcia@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Maria Castillo', '22','mcastillo@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Ethel Morales', '21','emorales@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Tania Saldaña', '21','tsaldaña@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Gustavo Saldaña', '22','gsaldana@gmail.com',TRUE);
INSERT INTO Clientes(nombre,edad,email,estatus) VALUES('Nancy Contreras', '22','ncontreras@gmail.com',TRUE);