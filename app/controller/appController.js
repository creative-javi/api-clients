/**
 * Este modulo sera el controlador con el cual se hacen las operaciones de la bd
 * tambien es el que se encarga de retornar las respuestas al cliente
 * Exporta las funciones que utiliza el ruteo
 */
'use strict'
const Cliente = require('../model/appModel'); //Importa el modulo del modelo

module.exports.list_all = (req, res) => { //Recibe la solicitud y una respuesta
    Cliente.getAllClientes((err, clientes) => { //Mandam un callback con el error en caso de haberlo y los resultados de la consulta
        if (err) //Revisa que no haya errores
        {
            res.status(res.status).send({ //En caso haber error manda una respuesta con el error interno 500 y el error.
                status: '500',
                message: `error: ${err}`
            });
        }
        else
        {
            res.send(clientes); //Regresa los clientes
        }
    });
};

//Crea un nuevo ciente y recibe la solicitud y la respuesya
module.exports.create_cliente = (req, res) => {
    let client = new Cliente(req.body); //Crea el objeto cliente.
    if (!client.nombre || !client.edad || !client.estatus) //Verifica que los campos requeridos por la db no vayan vacios de ir alguno vacio mandamos un error con el codigo 400
    {
        res.status(400).send({status: '400', message: "Asegurate de enviar todos los datos requeridos"});
    }
    else //Hace la peticion a la db para registrar al cliente
    {
        Cliente.createCliente(client,(err,cliente) => { //recibe el cliente y un callback el cual si no hubo errores notifica con el codigo 200
                                                        //En caso de haber error notifica con el codigo 500 y el error.
            if(err)
            {
                res.status(res.status).send({
                    status: '500',
                    message: `error: ${err}`
                });
            }
            else
            {
                res.send({
                    status: '200',
                    message: 'usuario registrado con exito'
                });
            }
        });
    }
    
};

//Consulta un cliente en especifico
//Recibe la solicitud y una respuesta.
module.exports.get_cliente = (req, res) => {
    let idCliente = req.body.idCliente; //obtiene el id del cliente con los parametros del body de la solicitud
    Cliente.getClienteById(idCliente, (err, cliente) => { //Recibe el id y un callback
        if (err) //verifica si hay un error y si lo hay notifica con el error 500
        {
            res.status(res.status).send({
                status: '500',
                message: `error: ${err}`
            });
        }
        else //de lo contrario envia como respuesta el cliente con un codigo 200. se accede por indices ya que el parametro del callback es un array.
        {
            res.send({
                status: '200',
                message: 'OK',
                id: cliente[0].idCliente,
                nombre: cliente[0].nombre,
                edad: cliente[0].edad,
                email: cliente[0].email,
                estatus: cliente[0].estatus
            });
        }
    });
};

//actualiza a un cliente. y recibe la solicitud y la respuesta que se envia.
module.exports.update_cliente = (req, res) => {
    let client = new Cliente(req.body); //Obtiene los datos del body y crea un cliente con los datos a actualizar.
    let id = req.body.idCliente; //obtiene el id del cliente.
    Cliente.updateClient(id,client, (err, cliente) => { // recibe el id, cliente y el callback para efectuar las respuestas
        if (err) //Revisa si hay errores y notifica con codigo 500
        {
            res.status(res.send).send({
                status: '500',
                message: `error: ${err}`
            });
        }
        else //si no hay errores notifica con codigo 200 que se efectuo correctamente la actualizacion 
        {
            res.send({
                status: '200',
                message: 'Se actualizo con exito el registro'
            });
        }
    });
};

//Elimina un cliente en especifico
//Recibe la solicitud y su respuesta.

module.exports.delete_cliente = (req, res) => {
    let idCliente = req.body.idCliente; //Obtiene el id del cliente desde los argumentos del body
    Cliente.deleteCliente(idCliente, (err, cliene) => { //Recibe el callback para las notificaciones.
        if (err) //Revisa si hay errores y notifica con codigo 500 en caso de ser verdad
        {
            res.status(res.status).send({
                status: '500',
                message: `error: ${err}`
            });
        }
        else //de lo contrario notifica con codigo 200
        {
            res.send({
                status: '200',
                message: 'Se elimino con exito el registro'
            });
        }
    });
};