/**
 * Este modulo servira para manejar las conexiones a la bd.
 */
'use estrict'
const mysql = require('mysql');
//Creamos la constante de conexion con los parametros tipicos de Mysql
const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'ISWHash2012Creative1220',
    database : 'ApiClientsDB'
});

//La funcion connect recibe un callback que se ejecuta y nos regresa una coneccion
//Si hay algun error lo notifica de lo contrario se conecta.
connection.connect((err) => {
    if (err) console.log(err);
    else console.log('conectado');
}); 

module.exports = connection; //Exportamos la conexion a la db