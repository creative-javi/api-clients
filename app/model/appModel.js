/**
 * Este modulo servira de modelo de los clientes 
 * contiene las operaciones CRUD
 */
'use estrict'
const sql = require('./db'); //importa el modulo de base de datos para manejar las operaciones a datos

//crea la clase cliente 
//Inicializa las propiedades del cliente
//Todas las funciones siguen el mismo patron que la primera por eso se omiten comentarios redundantes.
class Cliente {
    constructor(cliente) {
        this.nombre = cliente.nombre;
        this.edad = cliente.edad;
        this.email = cliente.email;
        this.estatus = cliente.estatus;
    }
    //declara la funcioon  para crear clientes en notación arrow.
    static createCliente(cliente, resultado) {
        sql.query('INSERT INTO `Clientes` SET ?', [cliente], (err, results, fields) => {
            if (err) //Si hubo algun error en la consulta se muestra y envia ejecuta el callback resultado
            {
                console.log(`Error: ${err}`);
                resultado(err, null); //envia el error generado.
            }
            else {
                console.log(`Resultado: ${results.last_inserted_id}`);
                resultado(null, results); //Como respuesta envia el ultimo id insertado
            }
        }); //Ejecuta la consulta sql y recibe como parametro un String con la consulta, los valores, y un callback que se ejecuta cuando termkina
    }
    static getAllClientes(resultado) {
        sql.query('SELECT * FROM Clientes', (err, results, fields) => {
            if (err) {
                console.log(`Error: ${err}`);
                resultados(err, null);
            }
            else {
                console.log(`Resultados: ${results}`);
                resultado(null, results);
            }
        });
    }
    static getClienteById(idCliente, resultado) {
        let query = 'select * from clientes where idCliente = ?';
        console.log(idCliente);
        sql.query(query, [idCliente], (err, results, fields) => {
            if (err) {
                console.log(`Error: ${err}`);
                resultados(err, null);
            }
            else {
                console.log(`Resultados: ${results}`);
                resultado(null, results);
            }
        });
    }
    static updateClient(id,cliente, resultado) {
        let query = 'update clientes set nombre = ?, edad = ?, email = ?, estatus = ? where idCliente = ?';
        sql.query(query, [cliente.nombre, cliente.edad, cliente.email, cliente.estatus, id], (err, results, fields) => {
            if (err) {
                console.log(`Error: ${err}`);
                resultados(err, null);
            }
            else {
                console.log(`Resultados: ${results}`);
                resultado(null, results);
            }
        });
    }
    static deleteCliente(clienteId, resultado) {
        let query = 'delete from clientes where idCliente  = ?';
        sql.query(query, [clienteId], (err, results, fields) => {
            if (err) {
                console.log(`Error: ${err}`);
                resultados(err, null);
            }
            else {
                console.log(`Resultados: ${results}`);
                resultado(null, results);
            }
        });
    }
}

module.exports = Cliente; //Exportamos el modulo.
