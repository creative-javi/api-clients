/**
 * En este modulo se crean las rutas de la aplicacion
 * los principales metodos de una api REST son
 * GET, POST, PUT, DELETE
 * 
 * Se definiran las rutas;
 * /clientes        METODOS : GET, POST
 * /clientes/clienteId  METODOS: GET, PUT, DELETE
 * 
 * la ruta '/clientes' sirve para lectura de todos los clientes y para crear uno nuevo
 * la ruta '/clientes/idCliente' se utiliza para lectura, actualizacion y eliminacion de elementos bajo un id dado.
 */
'use estrict'
module.exports = function(app)
{
    const controller = require('../controller/appController');

    //Ruteo de GET y POST para operaciones de lectura y escritura
    app.route('/clientes').get(controller.list_all);
    app.route('/create').post(controller.create_cliente);

    //Ruteo de GET, PUT y DELETE
    app.route('/delete/:id').delete(controller.delete_cliente);
    app.route('/cliente/:id').get(controller.get_cliente);
    app.route('/update/:id').put(controller.update_cliente);

} //Para trabajar con diferentes ficheros en nodejs se modularizan los archivos
    //Se exporta este modulo para posteirormentr