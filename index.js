/**
 * Archivo para el manejo del servidor con Express.js
 * Express es un framework para el manejo de http en nodeJS
 */
"use-strict" //Activa el modo estricto de javascript para evitar erroes de sintaxis
            //e.j. uso de palabras reservadas como identificadores.
const express = require('express');     //Se importa el modulo de express
const port = process.env.PORT || 3000;  //Se define el puerto
const app = express();                  //Se crea un Handler para manejar el modulo de express
const bodyParser = require('body-parser'); //Middleware para manejar los formatos de las solicitudes
const routes = require('./app/routes/appRoutes'); //importa el modulo del ruteo.

app.use(bodyParser.urlencoded({ extended: true })); //Usas los formatos de urlencoded
app.use(bodyParser.json()); //Usa el formato JSON

app.listen(port,() => {
    console.log("Server running...");
}); //Se hecha a andar el servidor en el puerto especificado
                  //O en el puerto por defecto del servidor.
routes(app); //Asignamos las rutas

                  
